'use strict';

var app = angular
  .module('sceApp', [
    'ngResource',
    'ngRoute'/*,
    'ngSanitize'*/ /* is responsible for cleaning and escaping any html input */
  ])
  .config(function ($routeProvider) {

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/href', {
        templateUrl: 'views/href.html',
        controller: 'HrefCtrl',
        controllerAs: 'href'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

app.config(['$sceProvider', '$sceDelegateProvider',function($sceProvider, $sceDelegateProvider){
  $sceProvider.enabled(true);

  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain.  Notice the difference between * and **.
    '*://www.youtube.com/**'
  ]);

}]);


